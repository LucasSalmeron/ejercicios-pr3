﻿using System;
using System.Net;
using System.IO;

using Newtonsoft.Json.Linq;
//newtonsoft.com/json  para bajar este programa
namespace Ejercicio1
{
    class Program
    {
        static void Main(string[] args)
        {
            string url = "http://www.omdbapi.com/?t=";
            bool salir = false;

            do
            {
                Console.WriteLine("INGRESE NOMBRE DE PELICULA O ESCRIBA OFF PARA SALIR");
                
                string pelicula = Console.ReadLine();
                pelicula = pelicula.ToLower();
                if (pelicula != "off")
                {
                    bool er = false;
                    string fecha = "";
                    try
                    {
                        WebRequest req = WebRequest.Create(url + pelicula);
                    //   WebRequest req = WebRequest.Create("http://www.omdbapi.com/?t=terminator");
                    WebResponse respuesta = req.GetResponse();

                    Stream stream = respuesta.GetResponseStream();

                    StreamReader sr = new StreamReader(stream);

                    JObject data = JObject.Parse(sr.ReadToEnd());

                    
                   
                        fecha = (string)data["Year"];
                    }catch(Exception e)
                    {

                        er = true;
                    }

                    if (!er && fecha != null)
                    {
                        Console.WriteLine("La fecha de estreno de la pelicula es " + fecha);
                    }else
                    {
                        if (er)
                        {
                            Console.WriteLine("ERROR: FALLO EN LA CONEXION");
                        }
                        else
                        {
                            Console.WriteLine("No se encuentra la pelicula");
                        }
                    }
                    Console.WriteLine("");
                }
                else
                {
                    salir = true;
                }
            } while (!salir);
            /* data importante: yo puedo bajar cualquier libreria de lo que sea 
               bajo una libreria, voy a mi version del framework (net 4.5 o la que sea), la agrego en referencias.
            */


        }

    }
}
