﻿using System;
using System.Net;
using System.IO;

using Newtonsoft.Json.Linq;
namespace Ejercicio2
{
    class Program
    {
        static void Main(string[] args)
        {
            string url = "https://maps.googleapis.com/maps/api/geocode/json?address=";
            bool salir = false;

            do
            {
                Console.WriteLine("INGRESE CIUDAD O ESCRIBA OFF PARA SALIR");

                string ciudad = Console.ReadLine();
                ciudad = ciudad.ToLower();
                if (ciudad != "off")
                {
                    string dato = "";
                    bool err = false;
                    StreamReader sr = null;
                    JObject data = null;
                    int i = -1;
                    try
                    {
                        WebRequest req = WebRequest.Create(url + ciudad);

                        WebResponse respuesta = req.GetResponse();

                        Stream stream = respuesta.GetResponseStream();

                        sr = new StreamReader(stream);
                    }catch(Exception e)
                    {
                        err = true;
                    }
                    if (!err)
                    {
                        data = JObject.Parse(sr.ReadToEnd());

                        
                        
                        while (dato != "country" && dato != "erroneo")
                        {
                            i++;
                            try
                            {
                                dato = (string)data["results"][0]["address_components"][i]["types"][0];
                            }
                            catch (Exception e)
                            {
                                dato = "erroneo";
                            }


                        }

                    }

                    if (err)
                    {
                        Console.WriteLine("ERROR: FALLO EN LA CONEXION");
                    }
                    else
                    {
                        if (dato != "erroneo")
                        {
                            string pais = (string)data["results"][0]["address_components"][i]["long_name"];
                            Console.WriteLine("El pais es " + pais);
                        }
                        else
                        {

                            Console.WriteLine("No se puede encontrar la ciudad");
                        }
                    }

                    Console.WriteLine("");
                }
                else
                {
                    salir = true;
                }
            } while (!salir);
           


        }

    }
}
